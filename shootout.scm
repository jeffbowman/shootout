#! /usr/bin/guile \
-e main -s
!#

;; (define-module (shootout)
;;   #:export (main))

(use-modules (srfi srfi-9)		; records
	     (ice-9 rdelim)		; %read-line
	     (ice-9 regex)		; string-match
	     (ice-9 eval-string)	; eval-string
	     )
             
;; record description for an order-line
(define-record-type order-line-type
  (make-order-line-type quantity product description line-number)
  order-line-type?
  (quantity    order-line-type-quantity)
  (product     order-line-type-product)
  (description order-line-type-description)
  (line-number order-line-type-line-number))

;; record description for an order
(define-record-type order-type
  (make-order-type order-number order-lines)
  order-type?
  (order-number order-type-order-number)
  (order-lines  order-type-order-lines))

(define order
  (lambda (ord-num . rest)
    "Make an order.

The ORD-NUM comes from the first form '(orderNumber) while REST is the
remaining arguments as a list. The remaining arguments will be
order-line-type values"
    
    (make-order-type ord-num rest)))

(define order-line
  (lambda (quantity product description line-number)
    "Make an order-line.

Returns an order-line-type from the provided arguments, which are
mandatory."
    (make-order-line-type quantity product description line-number)))

;; named identity functions, each is called when creating one of the
;; types above
(define (quantity q) q)
(define (product p) p)
(define (description d) d)
(define (line-number n) n)
(define (orderNumber n) n)

(define order-line->string
  (lambda (line)
    (let ((fields (list (order-line-type-line-number line)
			(order-line-type-product     line)
			(order-line-type-description line)
			(number->string (order-line-type-quantity line)))))
      (string-append "\""
		     (string-join fields "\",\"")
		     "\""))))

(define read-order-sexp
  (lambda (in sexp)
    "Read from port IN and populate SEXP.

Recursively reads from IN updating SEXP until the entire expression is
read. Initial calls should pass an empty string for SEXP. SEXP is
returned when it is complete."

    (let ((line-in (%read-line in)))
      (cond ((eof-object? (cdr line-in)) sexp)

	    ((string-match "^[)]$" (string-trim-right (car line-in)))
	     (string-append sexp ")"))	; complete sexp, add closing ) and return

	    ((string-match "^$" (string-trim-both (car line-in)))
	     (read-order-sexp in sexp))	; skip blank lines

	    (else (read-order-sexp in
			     (string-append sexp " "
					    (string-trim-both (car line-in)))))))))

(define make-order-list
  (lambda (in order-list)
    "Makes a list of order-type objects.

Reads from the port IN and populates LIST recursively as the port is
read."

    (let ((order-sexp (read-order-sexp in "")))
      (if (string-match "^$" order-sexp)
	  order-list
	  (make-order-list in
			   (append order-list
				   (list (eval-string order-sexp))))))))

(define read-orders
  (lambda (in)
    "Use port IN to read a list of orders"
    (make-order-list in '())))

(define write-header
  (lambda (out header)
    "Write the HEADER on the PORT."
    (display header out)
    (newline out)
    #f))

(define write-order-number
  (lambda (port order-num)
    "Writes the provided ORDER-NUM on the PORT.

The ORDER-NUM is wrapped in quotation marks on write."

    (display (string-append "\"" order-num "\"") port)
    (newline port)
    #f))

(define write-order-csv-file
  (lambda (order-list)
    "Write order number from ORDER-LIST to csv file 

ORDER-LIST should be just a list of order numbers as strings."

    (define write-orders
      (lambda (port)
        (write-header port "\"order_number\"")
	(map (lambda (num) (write-order-number port num) #f)
	     order-list)
	#f))

    (call-with-output-file "orders3.csv" write-orders)))

(define write-line-csv-file
  (lambda (orders)
    "Writes lines for each order in ORDERS to a csv file"

    (define write-line
      (lambda (port order-number line)
	"Writes a single order line."

	(display (string-append (order-line->string line)
				",\"" order-number "\"")
		 port)
	(newline port)
	#f))

    (define write-lines
      (lambda (port)
	"Writes all order-lines for all orders"

	(write-header port "\"line_number\",\"product\",\"description\",\"quantity\",\"order_number\"")
	(map (lambda (order)
	       (let ((lines (order-type-order-lines order))
		     (num (number->string (order-type-order-number order))))
		 (map (lambda (line) (write-line port num line)) lines))
	       #f)
	     orders)
	#f))
    
    (call-with-output-file "lines3.csv" write-lines)))

(define process-order-file
  (lambda (filename)
    "Process an input file named FILENAME.

Generates 2 csv files, one for lines and one for order numbers."
    
    (let ((order-list (call-with-input-file filename read-orders)))
      (write-order-csv-file
       (map number->string (map order-type-order-number order-list)))
      (write-line-csv-file order-list))))

(define main
  (lambda (args)
    "Main entry point, must provide a file name to process or an error
is thrown."
    
    (let ((filename
	   (if (> (length args) 1)
	       (cadr args)
	       (error "missing input orders file name to process"))))
      (process-order-file filename)
      (display "Done.")
      (newline))))
