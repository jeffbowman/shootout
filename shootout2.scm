#! /usr/bin/guile \
-e main -s
!#

(use-modules (srfi srfi-9)		; records
	     (ice-9 rdelim)		; %read-line
	     (ice-9 regex)		; string-match
	     (ice-9 eval-string)	; eval-string
	     )
             
;; record description for an order-line
(define-record-type order-line-type
  (make-order-line-type quantity product description line-number)
  order-line-type?
  (quantity    order-line-type-quantity)
  (product     order-line-type-product)
  (description order-line-type-description)
  (line-number order-line-type-line-number))

;; record description for an order
(define-record-type order-type
  (make-order-type order-number order-lines)
  order-type?
  (order-number order-type-order-number)
  (order-lines  order-type-order-lines))

(define order
  (lambda (ord-num . rest)
    "Make an order.

The ORD-NUM comes from the first form '(orderNumber) while REST is the
remaining arguments as a list. The remaining arguments will be
order-line-type values"
    
    (make-order-type ord-num rest)))

(define order-line
  (lambda (quantity product description line-number)
    "Make an order-line.

Returns an order-line-type from the provided arguments, which are
mandatory."
    (make-order-line-type quantity product description line-number)))

;; named identity functions, each is called when creating one of the
;; types above
(define (quantity q) q)
(define (product p) p)
(define (description d) d)
(define (line-number n) n)
(define (orderNumber n) n)

(define order-line->string
  (lambda (line)
    (let ((fields (list (order-line-type-line-number line)
			(order-line-type-product     line)
			(order-line-type-description line)
			(number->string (order-line-type-quantity line)))))
      (string-append (string #\") (string-join fields "\",\"") (string #\")))))

(define read-orders
  (lambda (in)
    "Read orders from port IN, return the list of them."

    (let ((order-list '())
	  (order-record (read in)))
      (while (not (eof-object? order-record))
	(set! order-list (append order-list (list (eval order-record (interaction-environment)))))
	(set! order-record (read in)))
      order-list)))

(define write-header
  (lambda (out header)
    "Write the HEADER on the PORT."
    (display header out)
    (newline out)
    #f))

(define write-order-csv-file
  (lambda (order-list)
    "Write order number from ORDER-LIST to csv file 

ORDER-LIST should be just a list of order numbers as strings."

    (define write-orders
      (lambda (port)
        (write "order_number" port)
	(newline port)
	(map (lambda (num) (write num port) (newline port) #f)
	     order-list)))

    (call-with-output-file "orders4.csv" write-orders)))

(define order-line->csv
  (lambda (line order-number)
    "Concatenate ORDER-NUMBER to LINE for output."
    (string-append (order-line->string line)
		    ",\"" order-number "\"" )))

(define write-line-csv-file
  (lambda (orders)
    "Writes lines for each order in ORDERS to a csv file"

    (define write-line
      (lambda (port order-number line)
	"Writes a single order line."

	(display (order-line->csv line order-number) port)
	(newline port)))

    (define write-lines
      (lambda (port)
	"Writes all order-lines for all orders"

	(write-header port "\"line_number\",\"product\",\"description\",\"quantity\",\"order_number\"")
	(map (lambda (order)
	       (let ((lines (order-type-order-lines order))
		     (num (number->string (order-type-order-number order))))
		 (map (lambda (line) (write-line port num line)) lines)))
	     orders)))
    
    (call-with-output-file "lines4.csv" write-lines)))

(define process-order-file
  (lambda (filename)
    "Process an input file named FILENAME.

Generates 2 csv files, one for lines and one for order numbers."
    
    (let ((order-list (call-with-input-file filename read-orders)))
      (write-order-csv-file
       (map number->string (map order-type-order-number order-list)))
      (write-line-csv-file order-list))))

(define main
  (lambda (args)
    "Main entry point, must provide a file name to process or an error
is thrown."
    
    (let ((filename
	   (if (> (length args) 1)
	       (cadr args)
	       (error "missing input orders file name to process"))))
      (process-order-file filename)
      (display "Done.")
      (newline))))
