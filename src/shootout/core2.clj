(ns shootout.core2
  (:require [clojure-csv.core :as csv]
            [clojure.java.io :as io])
  (:gen-class))

(defrecord order-line-type [quantity product description line-number])
(defrecord order-type [order-number order-lines])

(defn order [ord-num & rest]
  (order-type. ord-num rest))
(defn order-line [q p d l]
  (order-line-type. q p d l))
;; (defn quantity [num] num)
;; (defn product [prod-str] prod-str)
;; (defn description [desc-str] desc-str)
;; (defn line-number [num] num)
;; (defn orderNumber [num] num)

(def quantity (partial identity))
(def product (partial identity))
(def description (partial identity))
(def line-number (partial identity))
(def orderNumber (partial identity))

(def order-line-count 6)

(defmacro => [& body] `(do (in-ns 'shootout.core2) (eval (read-string ~@body))))

(defn read-to-sexp-end [lines]
  (loop [sub-lines lines
         sexp ""]
    (if (.matches (str (first sub-lines)) "^[)]$")
      (str sexp ")")
      (recur (rest sub-lines) (str sexp (first sub-lines))))))

(defn process-lines [lines]
  (loop [sub-lines lines
         orders nil]
    (if (empty? sub-lines) orders
        (if (.startsWith (str (.trim (first sub-lines))) "(order ")
          (let [order
                (=> (str (.replaceAll (read-to-sexp-end sub-lines) "\t" " ")))]
            (recur (drop (+ 2 (* order-line-count (count (:order-lines order))))
                         sub-lines)
                   (conj orders order)))
          (recur (rest sub-lines) orders)))))

(defn read-file [filename]
  (let [lines (with-open [r (io/reader filename)]
                (doall (line-seq r)))]
    (reverse (process-lines lines))))

(defn write-csv-files [orders]
  (with-open [o (io/writer "orders2.csv" :append false)
              ol (io/writer "lines2.csv" :append false)]
    (.write o (csv/write-csv [["order_number"]] :force-quote true))
    (.write ol (csv/write-csv [["line_number", "product", "description"
                                "quantity", "order_number"]]
                              :force-quote true))
    (doseq [order orders]
      (let [order-csv [[(str (:order-number order))]]
            order-lines (:order-lines order)
            line-vals (fn [line] [[(str (:line-number line))
                                   (:product line)
                                   (:description line)
                                   (str (:quantity line))
                                   (str (:order-number order))]])]
      (.write o (csv/write-csv order-csv :force-quote true))
      (doseq [line order-lines]
        (.write ol (csv/write-csv (line-vals line) :force-quote true)))))))

(defn -main [& args]
  (println "running core2")
  (let [fname (if (nil? (first args)) "orders2.txt" (first args))]
    (write-csv-files (read-file fname))))
