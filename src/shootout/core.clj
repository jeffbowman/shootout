(ns shootout.core
  (:require [clojure-csv.core :as csv]
            [clojure.java.io :as io])
  (:gen-class))

(defrecord order-line-type [quantity product description line-number])
(defrecord order-type [order-number order-lines])

(defn order-line [q p d l]
  (order-line-type. q p d l))
(defn quantity [num] num)
(defn product [prod-str] prod-str)
(defn description [desc-str] desc-str)
(defn line-number [num] num)
(defn orderNumber [num] num)

(def order-line-count 6)

(defmacro => [& body] `(do (in-ns 'shootout.core) (eval (read-string ~@body))))

(defn skip-to-order-number [s]
  (.substring s (.indexOf s "(orderNumber")))

(defn make-order-lines [lines order-lines]
  (if (or (empty? lines) (= ")" (first lines)))
    order-lines
    (let [olines1 (reduce str "" (take (- order-line-count 2) lines))
          olines2 (.replaceAll (first (drop (- order-line-count 2) lines))
                               "\"" "")
          olines (.replaceAll (str olines1 olines2 ")") "\t" " ")
          oline (=> olines)]
      (recur (drop order-line-count lines)
             (conj order-lines oline)))))

(defn process-lines [lines orders]
  (if (empty? lines)
    orders
    (let [line (first lines)]
      (if (.startsWith (.trim (str line)) "(order ")
        (let [onum (=> (skip-to-order-number line))
              olines (make-order-lines (rest lines) [])
              order (order-type. onum olines)]
          (recur (drop (* order-line-count (count olines)) lines)
                 (conj orders order)))
        (recur (rest lines) orders)))))

(defn read-file [filename]
  (let [lines (with-open [r (io/reader filename)]
                (doall (line-seq r)))]
    (process-lines lines [])))

(defn write-csv-files [orders]
  (with-open [o (io/writer "orders.csv" :append false)
              ol (io/writer "lines.csv" :append false)]
    (.write o (csv/write-csv [["order_number"]] :force-quote true))
    (.write ol (csv/write-csv [["line_number", "product", "description"
                                "quantity", "order_number"]]
                              :force-quote true))
    (doseq [order orders]
      (let [order-csv [[(str (:order-number order))]]
            order-lines (:order-lines order)
            line-vals (fn [line] [[(str (:line-number line))
                                   (:product line)
                                   (:description line)
                                   (str (:quantity line))
                                   (str (:order-number order))]])]
      (.write o (csv/write-csv order-csv :force-quote true))
      (doseq [line order-lines]
        (.write ol (csv/write-csv (line-vals line) :force-quote true)))))))

(defn -main [& args]
  (println "running core")
  (let [fname (if (nil? (first args)) "orders.txt" (first args))]
    (write-csv-files (read-file fname))))
