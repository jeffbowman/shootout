# update

* fixed orders.txt to not have the missing double-quote on
  line-numbers
* added core2.clj to use a nicer dsl to "execute" orders2.txt when it
  is read
* added profiles to allow running either the original code or the
  newer code (see below)

# shootout

The OKC JUG presented a topic for a language shootout which included a
problem and a solution by various languages, notably java, groovy,
scala and c#. Clojure was not represented, so I thought I'd give it a
try. This is the result. The problem description is below.

[Problem Description](https://docs.google.com/document/d/1IGotMoOOoRs_KLZbbGUkg_i9koRIak3EXFYaHi281kI "Original example data document") (example data in orders.txt)

A Clojure library designed to transform NoSQL data formateted as below
into a csv file which could be read into a database. Duplicates should
be removed for products, customers and addresses.

    (order
        (order-number 2345)
        (order-line
                 (quantity 10)
                 (product “FXSIEJFI”)
                 (description “Rain Coat”)
                 (line-number 1))
        (order-line
                 (quantity 3)
                 (product “FJEISL”)
                 (description “Boot”)
                 (line-number 2)))

Notional database

    CREATE TABLE Order (
        order_number int not null,
        PRIMARY KEY (order_number)
    )

    CREATE TABLE OrderLine (
        line_number int not null,
        product varchar(255),
        description varchar(500)
        quantity int,
        order_number int,
        PRIMARY KEY (line_number)
        FOREIGN KEY (order_number) REFERENCES Order (order_number)
    )


## Usage

To run this code, you'll need to install the
[Leiningen](http://leiningen.org/) build tool for clojure. It runs in
the clojure repl as demonstrated below. The output files are for the
notional database described above, so there will be an orders.csv file
and a lines.csv file generated. It is possible to run the program
from the command line with:

    lein main-1
    lein main-2

The first profile (main-1) runs with the original code and the
"broken" orders.txt file. The original orders.txt file had a missing
double-quote on the line-number forms (or possibly an additional one -
depending on your point of view). The orders2.txt file has a fix which
adds the double-quote on the line-number forms.

The overall effect was to refactor core.clj to core2.clj and write the
code with more of a dsl which just executes the file as it is
read. The original solution (core.clj) had to handle the missing
double-quote, so there was more "data cleanup" type code before the
dsl can be used.

## License

There is no license, do what you want to.
