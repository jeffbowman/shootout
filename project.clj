(defproject shootout "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.10.0"]
                 [clojure-csv/clojure-csv "2.0.2"]]
  :profiles {:main-1 {:main shootout.core}
             :main-2 {:main shootout.core2}}
  :aliases {"main-1" ["with-profile" "main-1" "run"]
            "main-2" ["with-profile" "main-2" "run"]})
